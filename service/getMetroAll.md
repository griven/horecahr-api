##service.getMetroAll - Получение списка всех метро

Запрос
```
{"jsonrpc":"2.0","method":"service.getMetroAll","params":[{"token":"6frkByk3gJtjwBn9GCqoOpwH5E_e5Ar3"}],"id":"14"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "14",
  "result": [
    {
      "id": "101",
      "line_id": "1",
      "name": "Гражданский пр.",
      "line_color": "#EF1E25"
    },
    {
      "id": "102",
      "line_id": "1",
      "name": "Академическая",
      "line_color": "#EF1E26"
    },
    ...
}
```