##service.getMetroLines - Получение списка линий метро вместе со станциями

Запрос
```
{"jsonrpc":"2.0","method":"service.getMetroLines","params":[{"token":"6frkByk3gJtjwBn9GCqoOpwH5E_e5Ar3"}],"id":"74"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "74",
  "result": [
    {
      "id": 1,
      "name": "Красная ветка",
      "line_color": "#EF1E25",
      "stations": [
        "Гражданский пр.",
        "Академическая",
        "Политехническая",
        ...
      ]
    },
    {
      "id": 2,
      "name": "Синяя ветка",
      "line_color": "#019EE0",
      "stations": [
        "Парнас",
        "Пр. просвещения",
        "Озерки",
        ...
      ]
    },
    ...
  ]
}
```