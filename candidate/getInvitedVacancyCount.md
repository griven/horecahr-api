##candidate.getInvitedVacancyCount - Получение количества вакансий отзыв специалиста на которые был принят

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.getInvitedVacancyCount", "params": [{ "token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": 2
}
```