##candidate.get                   - Получение информации о кандидате

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.get", "params": [{ "token" : "aO2O-G3kPiumfBTR00oLZnKCh23-7Q_Y"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "user_id": "3",
    "name": "Константин",
    "surname": "Васильевsd",
    "middlename": "Иванович",
    "birthdate": "1980-05-29",
    "city": "Санкт-Петербург",
    "phone": "+7 (222) 22s2-22-24",
    "would_like_to_work": "1",
    "rating": 4,
    "balance": 0,
    "region": "Ленинградскаяd",
    "comment": "nulssla",
    "job_types": [
      {
        "id": 1,
        "name": "Повар"
      },
      {
        "id": 2,
        "name": "Официант"
      }
    ],
    "intervals": [
      {
        "id": "8",
        "name": null,
        "start_at": "2015-03-14 23:00:00",
        "finish_at": "0000-00-00 00:00:00"
      },
      {
        "id": "9",
        "name": "еще один интервал",
        "start_at": "2015-03-14 23:00:00",
        "finish_at": "2015-03-14 23:00:00"
      }
    ],
    "avatar_src": "http://hr.dev//uploads/files/6/2016/03/21/56efb4136174d.jpeg"
  }
}
```