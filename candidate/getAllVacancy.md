##candidate.getAllVacancy - Получение списка все вакансий на которые откликался кандидат

###параметры:
- id_from - число, необязательный. Id последней вакансии,после которой требуется получить новые вакансии.

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.getAllVacancy", "params": [{ "token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "1",
      "salary": "200",
      "salary_type": "1",
      "start_at": "2015-12-22 13:27:21",
      "finish_at": "2015-12-25 13:15:40",
      "status": "2",
      "house_id": "1",
      "employer_id": "3",
      "position_name": "Повар",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    }
  ]
}
```