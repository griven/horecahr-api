##candidate.set                   - Добавление/обновление информации о кандидате

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.set", "params": [{ "token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "name" : "vasya12"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "name": "vasya12"
  }
}
```