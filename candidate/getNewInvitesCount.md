##candidate.getNewInvitesCount - Получение количества новых/непросмотренных приглашений

Запрос
```
{"jsonrpc":"2.0","method":"candidate.getNewInvitesCount","params":[{"token":"6frkByk3gJtjwBn9GCqoOpwH5E_e5Ar3"}],"id":"74"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "74",
  "result": "2"
}
```