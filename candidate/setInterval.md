##candidate.setInterval                   - Добавление интервала

###параметры:
- start_at - дата, обязательный. Начало интервала
- finish_at - дата, обязательный. Конец интервала
- name - строка, необязательный. Название интервала

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.setInterval", "params": [{ "token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "start_at" : "2015-03-14 23:00:00", "finish_at" : "15-03-14 23:00:00", "name" : "еще один интервал"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": 9
}
```