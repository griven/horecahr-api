##candidate.getAllVacancyCount - Получение количества все вакансий на которые откликался кандидат

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.getAllVacancyCount", "params": [{ "token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": 3
}
```