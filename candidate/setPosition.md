##candidate.setPosition                   - Добавление должности/позиции

###параметры:
- position_id - число, обязательный. Id должности/позиции

Запрос
```
{"jsonrpc":"2.0","method":"candidate.setPosition","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "position_id":2}],"id":"10"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "10",
  "result": true
}
```

Неудачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "10",
  "error": {
    "code": 0,
    "message": "error",
    "data": "User already have this Position"
  }
}
```