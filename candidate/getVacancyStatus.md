##candidate.getVacancyStatus         - Получение статуса вакансии для кандидата

Запрос
```
{"jsonrpc": "2.0", "method": "candidate.getVacancyStatus", "params": [{ "token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "vacancy_id" : 41}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": 5
}
```