##candidate.deleteInterval                   - Удаление интервала

###параметры:
- interval_id - число, обязательный. Id интервала

Запрос
```
{"jsonrpc":"2.0","method":"candidate.deleteInterval","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "interval_id" : "8"}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": true
}
```

Неудачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "error": {
    "code": 0,
    "message": "error",
    "data": "not found"
  }
}
```