API HorecaHR
=========================
Большинство методов передается в формате json-rpc 2.0, однако есть отдельные случаи (в основном загрузка файлов), 
которые передаются обычным POST методом и по отдельному URL. В описании таких методов будет присутствовать слово POST.

Шаблона запроса в формате json-rpc 2.0
```
{"jsonrpc": "2.0", "method": "method.name", "params": [{"param1": 1, "param2": "dva"}], "id": 1}
```

Шаблон удачного ответа
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "somedata": "somedata"
  }
}
```

Шаблон неудачного(ошибка) ответа, параметр data необязательный
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "error": {
    "code": 0,
    "message": "error message",
    "data": {
      "somedata": "somedata"
    }
  }
}
```

Шаблон POST запроса
```
token: secret_token_here
method: group.Method
param1: 123
param2: abc
```

Поддерживаемые методы API
=========================

##            Пользователь :
- [user.login](user/login.md)       - Авторизация пользователя
- [user.logout](user/logout.md)     - Отмена авторизации
- [user.recovery](user/recovery.md) - Восстановление пароля
- [user.setPushToken](user/setPushToken.md) - Установка пуш токена
- [user.addFavorite](user/addFavorite.md) - Добавление пользователя в избранное
- [user.delFavorite](user/delFavorite.md) - Удаление пользователя из избранного

##            Вакансии :
- [vacancy.create](vacancy/create.md)               - Создание вакансии
- [vacancy.createFromArchive](vacancy/createFromArchive.md)  - Создание вакансии из архивной
- [vacancy.update](vacancy/update.md)               - Обновление вакансии
- [vacancy.delete](vacancy/delete.md)               - Удаление вакансии
- [vacancy.archive](vacancy/archive.md)             - Архивирование вакансии
- [vacancy.list](vacancy/list.md)                   - Получение списка вакансий по фильтру
- [vacancy.listArchive](vacancy/listArchive.md)     - Получение списка архивных вакансий по фильтру
- [vacancy.mapList](vacancy/mapList.md)             - Получение списка вакансий по координатам для карты
- [vacancy.getPositions](vacancy/getPositions.md)   - Получение списка позиций(должностей)
- [vacancy.getExtra](vacancy/getExtra.md)           - Получение дополнительной информации по вакансии
- [vacancy.getExtraForOwner](vacancy/getExtraForOwner.md) - Получение дополнительной информации по вакансии
- [vacancy.getTimeForReply](vacancy/getTimeForReply.md)  - Получить время для отклика

##            Отклики(Заявки на вакансии) :
- [reply.create](reply/create.md)   - Создание отклика специалистом
- [reply.decline](reply/decline.md)  - отклонение отклика специалиста заведением

##            [Приглашения](invite/index.md) :
- [invite.create](invite/create.md)     - Создание приглашения специалиста заведением
- invite.get                            - Получение информации специалистом или заведением о приглашении
- invite.confirm                        - Подтверждение приглашения  заведения специалистом
- [invite.decline](invite/decline.md)   - Отклонение приглашения заведения специалистом
- [invite.declineByOwner](invite/declineByOwner.md) - Отклонение приглашения заведения специалистом
- invite.list                           - Получить список приглашений по фильтру
- invite.getByHouse                     - Получить список приглашений по заведению
- invite.getBySpecialist                - Получить список приглашений по специалисту

##            [Работа](job/index.md) :
- job.started                     - Специалист выполнил свои обязательства по приглашению, Работа начата
- job.impossible                  - Специалист не выполнил свои обязательства по приглашению , Работа не может быть начата
- job.feedback                    - Отклик специалиста или заведения о результатах работы

##            Работодатель:
- [employer.get](employer/get.md)                                       - Получение информации о заведении
- [employer.getTypes](employer/getTypes.md)                             - Получение типов заведений
- [employer.getCandidate](employer/getCandidate.md)                     - Получение информации о кандидате
- [employer.getInvitedCandidates](employer/getInvitedCandidates.md)     - Получение информации о приглашенных кандидатах
- [employer.getRepliedCandidates](employer/getRepliedCandidates.md)     - Получение информации откликнувшихся на вакансию кандидатов
- [employer.getFavoriteCandidates](employer/getFavoriteCandidates.md)   - Получение информации об избранных кандидатах
- [employer.set](employer/set.md)                                       - Добавление/обновление информации о заведении
- [employer.setPhotos](employer/setPhotos.md)                           - POST Добавление фотографий заведения
- [employer.getWithoutReplyVacancy](employer/getWithoutReplyVacancy.md) - Получение списка вакансий на которые нет откликов
- [employer.getRepliedVacancy](employer/getRepliedVacancy.md)           - Получение списка вакансий на которые есть отклики
- [employer.getConfirmedVacancy](employer/getConfirmedVacancy.md)       - Получение списка вакансий с подтвержденым кандидатом

##            Кандидат:
- [candidate.get](candidate/get.md)                 - Получение информации о кандидате
- [candidate.set](candidate/set.md)                 - Добавление/обновление информации о кандидате
- [candidate.setPhoto](candidate/setPhoto.md)       - POST Добавление/обновление фотографии кандидата
- [candidate.setInterval](candidate/setInterval.md)         - Добавление интервала
- [candidate.deleteInterval](candidate/deleteInterval.md)   - Удаление интервала
- [candidate.setPosition](candidate/setPosition.md)         - Добавление должности/позиции
- [candidate.deletePosition](candidate/deletePosition.md)   - Удаление должности/позиции
- [candidate.getAllVacancy](candidate/getAllVacancy.md)           - Получение списка всех вакансий на которые откликался кандидат
- [candidate.getDeclinedVacancy](candidate/getDeclinedVacancy.md) - Получение списка вакансий отзыв специалиста на которые был отклонен
- [candidate.getInvitedVacancy](candidate/getInvitedVacancy.md)   - Получение списка вакансий отзыв специалиста на которые был принят
- [candidate.getInviteConfirmedVacancy](candidate/getInviteConfirmedVacancy.md) - Получение списка вакансий приглашение на которые было подтверждено
- [candidate.getVacancyStatus](candidate/getVacancyStatus.md)     - Получение статуса вакансии для кандидата
- [candidate.getAllVacancyCount](candidate/getAllVacancyCount.md)           - Получение количества всех вакансий на которые откликался кандидат
- [candidate.getInvitedVacancyCount](candidate/getInvitedVacancyCount.md)   - Получение количества вакансий отзыв специалиста на которые был принят
- [candidate.getNewInvitesCount](candidate/getNewInvitesCount.md) - Получение количества новых/непросмотренных приглашений

##            Служба поддержки:
- [support.info](support/info.md)       -Получение информации о службе поддержки
- [support.message](support/message.md) -Отправка сообщения в службу поддержки

##            Отзывы
- [rating.create](rating/create.md)     - Создание отзыва
- [rating.list](rating/list.md)         - Получение списка отзывов

##            Служебные команды:
- [service.getMetroAll](service/getMetroAll.md) - Получение списка всех метро
- [service.getMetroLines](service/getMetroLines.md) - Получение списка линий метро вместе со станциями