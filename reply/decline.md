##reply.decline                   - отклонение отклика специалиста заведением

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "reply.decline", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST",
        "candidate_id":2,
        "vacancy_id" : 2
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": true
}
```