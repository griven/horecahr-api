##reply.create                    - Создание отклика специалистом

Запрос
```
{"jsonrpc":"2.0","method":"reply.create","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "vacancy_id" : 3}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": 7
}
```