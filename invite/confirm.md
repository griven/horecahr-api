##invite.confirm                  - Принятие приглашения заведения специалистом

Запрос
```
{"jsonrpc": "2.0", "method": "invite.confirm", "params": [{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "vacancy_id" : 1}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": true
}
```
