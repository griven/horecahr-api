##invite.declineByOwner  - Отклонение своего приглашения заведением

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "invite.declineByOwner", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST",
        "candidate_id" : 2,
        "vacancy_id" : 2
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": true
}
```
