##invite.create                   - Создание приглашения специалиста заведением

###параметры:
- candidate_id - число, обязательный. ID пользователя которого приглашают на вакансию
- vacancy_id - число, обязательный. ID вакансии на которую приглашают

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "invite.create", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST",
        "candidate_id":2,
        "vacancy_id" : 2
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": 5
}
```