
invite.get                      - Получение информации специалистом или заведением о приглашении

Запрос
{"jsonrpc": "2.0", "method": "invite.get", "params": [{ "id" : 13}], "id": 1}

Удачный ответ
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "id": "1",
    "reply_id": "10",
    "vacancy_id": "1",
    "specialist_id": "0",
    "house_id": "1",
    "canceled_state": "0",
    "completed_state": null,
    "updated_at": "2015-11-30 11:43:43",
    "created_at": "2015-11-25 16:59:48",
    "status": "0",
    "last_decline_time": "0000-00-00 00:00:00",
    "feedback_by_specialist": "какой-то текст",
    "feedback_by_house": "еще какой-то текст"
  }
}


-----------------------------------------------------------------------------------------------

invite.confirm                  - Подтверждение приглашения  заведения специалистом

Запрос
{"jsonrpc": "2.0", "method": "invite.confirm", "params": [{ "id" : 13}], "id": 1}

Удачный ответ
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": true
}

-----------------------------------------------------------------------------------------------

invite.list                  - Получить список приглашений по фильтру

Запрос
{"jsonrpc": "2.0", "method": "invite.list", "params": [{ "specialist_id" : 2}], "id": 1}

Удачный ответ
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "17",
      "reply_id": "13",
      "vacancy_id": "12",
      "specialist_id": "2",
      "house_id": "2",
      "canceled_state": null,
      "completed_state": null,
      "updated_at": "2015-11-30 18:05:52",
      "created_at": "2015-11-30 18:05:52",
      "status": null,
      "last_decline_time": "0000-00-00 00:00:00",
      "feedback_by_specialist": null,
      "feedback_by_house": null
    }
  ]
}

-----------------------------------------------------------------------------------------------

invite.getByHouse               - Получить список приглашений по заведению

Запрос
{"jsonrpc": "2.0", "method": "invite.getByHouse", "params": [{ "id" : 1}], "id": 1}

Удачный ответ
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "1",
      "reply_id": "10",
      "vacancy_id": "1",
      "specialist_id": "0",
      "house_id": "1",
      "canceled_state": "0",
      "completed_state": null,
      "updated_at": "2015-11-30 11:43:43",
      "created_at": "2015-11-25 16:59:48",
      "status": "0",
      "last_decline_time": "0000-00-00 00:00:00",
      "feedback_by_specialist": "text",
      "feedback_by_house": "text"
    },
    {
      "id": "2",
      "reply_id": "10",
      "vacancy_id": "1",
      "specialist_id": "0",
      "house_id": "1",
      "canceled_state": "0",
      "completed_state": null,
      "updated_at": "2015-11-27 15:40:39",
      "created_at": "2015-11-25 17:04:27",
      "status": "1",
      "last_decline_time": "0000-00-00 00:00:00",
      "feedback_by_specialist": null,
      "feedback_by_house": null
    }
  ]
}

-----------------------------------------------------------------------------------------------

invite.getBySpecialist          - Получить список приглашений по специалисту

Запрос
{"jsonrpc": "2.0", "method": "invite.getBySpecialist", "params": [{ "id" : 0}], "id": 1}

Удачный ответ
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "1",
      "reply_id": "10",
      "vacancy_id": "1",
      "specialist_id": "0",
      "house_id": "1",
      "canceled_state": "0",
      "completed_state": null,
      "updated_at": "2015-11-30 11:43:43",
      "created_at": "2015-11-25 16:59:48",
      "status": "0",
      "last_decline_time": "0000-00-00 00:00:00",
      "feedback_by_specialist": "text",
      "feedback_by_house": "text"
    },
    {
      "id": "2",
      "reply_id": "10",
      "vacancy_id": "1",
      "specialist_id": "0",
      "house_id": "1",
      "canceled_state": "0",
      "completed_state": null,
      "updated_at": "2015-11-27 15:40:39",
      "created_at": "2015-11-25 17:04:27",
      "status": "1",
      "last_decline_time": "0000-00-00 00:00:00",
      "feedback_by_specialist": null,
      "feedback_by_house": null
    }
  ]
}