##vacancy.list                    - Метод получения списка вакансий по фильтру

###параметры:
- page - число, необязательный. Номер страницы
- limit - число, необязательный. Количество объектов на странице
- position_id - число, необязательный. Id должности
- employer_id - число, необязательный. Id работодателя (на данный момент не требуется в приложении)
- salary_type - число, необязательный. Тип оплаты (на данный момент не требуется в приложении)
- status - число, необязательный. Статус вакансии 
STFC_WITHOUT_INVITE = 0; (без приглашения)
STFC_WITH_INVITE = 1; (с приглашением, т/е исполнитель уже выбран)
STFC_CLOSED = 2; (закрытая)
- start_after - time "02:00:00", необязательный. Время начала вакансий должно быть равно либо позже заданного
- finish_before - time "14:00", необязательный. Время окончания вакансий должно быть равно либо раньше заданного
- id_from - число, необязательный. Id последней вакансии,после которой требуется получить новые вакансии.
- vacancy_id - число,необязательный. Id вакансии которую необходимо получить
- metro - число либо массив чисел, необязательный. Id метро у которых искать вакансии
- search_string - строка, необязательный. Поисковая строка

Запрос
```
{"jsonrpc": "2.0", "method": "vacancy.list", "params": [{ "token": "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "page": 1,"limit" : 2}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "12",
      "house_id": "1",
      "salary": "200",
      "salary_type": "0",
      "start_at": "2015-12-15 02:19:18",
      "finish_at": "2015-12-16 00:00:00",
      "status": "1",
      "position_name": "Официант",
      "work_dates": [
          {
            "id": "1",
            "date": "2016-01-25 11:10:02",
            "weekday": "1"
          }
      ],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3",
      "metro": [
          "Академическая",
          "Пл. мужества"
      ]
    },
    {
      "id": "11",
      "house_id": "1",
      "salary": "200",
      "salary_type": "1",
      "start_at": "2015-12-14 00:00:00",
      "finish_at": "2015-12-15 00:00:00",
      "status": "2",
      "position_name": "Повар",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3",
      "metro": [
        "Академическая",
        "Пл. мужества"
      ]
    }
  ]
}
```