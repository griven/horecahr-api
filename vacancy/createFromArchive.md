##vacancy.createFromArchive  - Создание вакансии из архивной

###параметры:
- vacancy_id - число, обязательный. Номер архивной вакансии

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "vacancy.createFromArchive", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST",
        "vacancy_id" : 7
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": true
}
```