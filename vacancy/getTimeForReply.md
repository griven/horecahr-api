##vacancy.getTimeForReply            - Получить время для отклика

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "vacancy.getTimeForReply", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST",
        "vacancy_id" : 2
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": {
    "moreDay": "23 дня"
  }
}
```