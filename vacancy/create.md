##vacancy.create                  - Метод создания вакансии заведением

###параметры:
- position_id - число, обязательный. Номер должности/позиции
- salary_type - число, обязательный. Тип оплаты
- salary - число, обязательный. Заработок
- description - текст, необязательный. Описание
- start_at - дата, необязательный. Начало работы
- finish_at - дата, необязательный (обязательный если есть start_at). Окончание работы
- city_id - число, необязательный. Номер города

Запрос
```
{"jsonrpc": "2.0", "method": "vacancy.create", "params": [{ "token" : "xpIArzV-guJ50GfINq1ESUGR5gInGrST", "position_id" : "1", "salary_type" : 2, "vacancy_id" : 2, "salary": 2000, "status" : 1, "start_at": "2016-12-22 11:30:00", "finish_at": "2016-12-22 16:00:00", "condition" : "условия работы"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": 12
}
```

Ошибка
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "error": {
    "code": 0,
    "message": "error",
    "data": {
      "position_id": [
        "Position ID is invalid."
      ]
    }
  }
}
```