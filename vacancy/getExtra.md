##vacancy.getExtra - Получение дополнительной информации по вакансии

###параметры:
- vacancy_id - число, обязательный. Номер вакансии

Запрос
```
{"jsonrpc":"2.0","method":"vacancy.getExtra","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS","vacancy_id":1}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": {
    "count": 3,
    "rating": 3,
    "links": [],
    "status": 2
  }
}
```