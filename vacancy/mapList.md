##vacancy.mapList                    - Метод получения списка вакансий по координатам для карты

###параметры:
- nw_lat - широта северо-западного угла
- nw_lon - долгота северо-западного угла
- se_lat - широта юго-западного угла
- se_lon - долгота юго-западного угла
остальные как в [vacancy.list](list.md)

Запрос
```
{"jsonrpc":"2.0","method":"vacancy.mapList","params":[{"token":"6frkByk3gJtjwBn9GCqoOpwH5E_e5Ar3", "nw_lat" : 100, "nw_lon" : 100, "se_lat" : 59.90, "se_lon" : 3}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": [
    {
      "coordinates": [
        [
          59.93457,
          30.331857
        ]
      ],
      "items": [
        {
          "id": 6,
          "salary": 123,
          "salary_type": 1,
          "start_at": "1970-01-01 00:00:00",
          "finish_at": "1970-01-01 00:00:00",
          "status": 5,
          "house_id": 1,
          "position_name": "Повар",
          "work_dates": [],
          "metro": [
            "Гражданский пр.",
            "Академическая"
          ],
          "title": "На невском",
          "address": "Невский 123",
          "latitude": 59.93457,
          "longitude": 30.331857
        },
        {
          "id": 4,
          "salary": 123,
          "salary_type": 1,
          "start_at": "1970-01-01 00:00:00",
          "finish_at": "1970-01-01 00:00:00",
          "status": 2,
          "house_id": 1,
          "position_name": "Повар",
          "work_dates": [],
          "metro": [
            "Гражданский пр.",
            "Академическая"
          ],
          "title": "На невском",
          "address": "Невский 123",
          "latitude": 59.93457,
          "longitude": 30.331857
        }
      ]
    }
  ]
}
```