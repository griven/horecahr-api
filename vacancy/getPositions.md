##vacancy.getPositions            - Получение списка позиций(должностей)

Запрос
```
{"jsonrpc": "2.0", "method": "vacancy.getPositions", "params": [{ "token": "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "1",
      "name": "Повар"
    },
    {
      "id": "2",
      "name": "Официант"
    }
  ]
}
```