##vacancy.getExtraForOwner - Получение дополнительной информации по вакансии для владельца

###параметры:
- vacancy_id - число, обязательный. Номер вакансии

Запрос
```
{"jsonrpc": "2.0", "method": "vacancy.getExtraForOwner", "params": [{ "token" : "xpIArzV-guJ50GfINq1ESUGR5gInGrST", "vacancy_id" : 4}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "can_update": true,
    "description": "Здесь будет описание вакансии",
    "requirements": "От вас почти ничего не требуется. Главное дойти до работы.",
    "condition": "Бесподобные условия работы в нашем элитном заведении",
    "replies_count": "1",
    "candidate": {
      "name": "Артур Пирожков",
      "position": "Официант",
      "age": 13,
      "is_favorite": false
    }
  }
}
```