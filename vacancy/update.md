##vacancy.update                  - Обновление вакансии

###параметры:
- vacancy_id - число, обязательный. Номер вакансии
- position_id - число, необязательный. Номер должности/позиции
- salary_type - число, необязательный. Тип оплаты
- salary - число, необязательный. Заработок
- description - текст, необязательный. Описание
- start_at - дата, необязательный. Начало работы
- finish_at - дата, необязательный. Окончание работы
- city_id - число, необязательный. Номер города

Запрос
```
{"jsonrpc": "2.0", "method": "vacancy.update", "params": [{ "token" : "Iz2GoecY2t-NS3OukHjvB1h_XNgO68eP", "position_id" : "1", "salary_type" : 2, "vacancy_id" : 2, "salary": 2000, "status" : 1, "finish_at": "2016-12-22"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "position_id": 1,
    "finish_at": "2016-12-22",
    "salary_type": 3
  }
}
```

Ошибка
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "error": {
    "code": 0,
    "message": "error",
    "data": {
      "position_id": [
        "Position ID is invalid."
      ]
    }
  }
}
```