##vacancy.delete               - Удаление вакансии

###параметры:
- vacancy_id - число, обязательный. Номер вакансии

Запрос
```
{"jsonrpc": "2.0", "method": "vacancy.delete", "params": [{ "token" : "xpIArzV-guJ50GfINq1ESUGR5gInGrST", "vacancy_id" : 4}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": true
}
```

Ошибка
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "error": {
    "code": 0,
    "message": "error",
    "data": "can't find"
  }
}
```