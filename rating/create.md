##rating.create                  - Создание отзыва

###параметры:
- target_id - число, обязательный. Номер заведения на которое ставиться отзыв(рейтинг)
- rating - число, обязательный. Оценка по пятибальной шкале (от 1 до 5)
- comment - строка, необязательный. Комментарий к отзыву

Запрос
```
{"jsonrpc":"2.0","method":"rating.create","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "rating" : 1, "target_id" : 1, "comment" : "abc"}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": {
    "rating_id": 51,
    "created_at": "2016-01-19 17-46-02"
  }
}
```

Неудачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "error": {
    "code": 8,
    "message": "Undefined index: rating"
  }
}
```