##rating.list         - Получение списка отзывов

###параметры:
- user_id - число, обязательный. Id пользователя, отзывы на которого надо найти
- id_from - число, необязательный. Id последнего отзыва,после которой требуется получить новые

Запрос
```
{"jsonrpc":"2.0","method":"rating.list","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS",  "user_id" : 3}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": [
    {
      "id": "41",
      "user_name": "vasya12 ",
      "created_at": "2016-01-19 16:49:34",
      "comment": "avx"
    }
  ]
}
```
