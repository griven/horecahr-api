##employer.getInvitedCandidates  - Получение информации о приглашенных кандидатах

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "employer.getInvitedCandidates", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST"
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": [
    {
      "id": 2,
      "name": "Артур Пирожков",
      "avatar_src": "http://hr.dev//uploads/files/6/2016/04/11/570bbb5675585.jpeg",
      "age": 13,
      "position": "Официант",
      "is_favorite": true,
      "vacancy_id": "4",
      "confirm_end_date": "2016-05-23 15:11:01"
    },
    {
      "id": 2,
      "name": "Артур Пирожков",
      "avatar_src": "http://hr.dev//uploads/files/6/2016/04/11/570bbb5675585.jpeg",
      "age": 13,
      "position": "Официант",
      "is_favorite": true,
      "vacancy_id": "5",
      "confirm_end_date": "2016-05-23 15:11:01"
    }
  ]
}
```