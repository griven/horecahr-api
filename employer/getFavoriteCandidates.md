##employer.getFavoriteCandidates  - Получение информации об избранных кандидатах

###параметры:
-only_worked - число, необязательный. Выбрать только работавших ранее

Запрос
```
{
    "jsonrpc": "2.0", 
    "method": "employer.getFavoriteCandidates", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST"
      }
    ],
    "id": 3
}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 3,
  "result": [
    {
      "id": "2",
      "name": "Артур Пирожков",
      "avatar_src": "http://hr.dev//uploads/files/6/2016/04/11/570bbb5675585.jpeg",
      "age": 13,
      "position": "Официант",
      "is_favorite": true
    }
  ]
}
```