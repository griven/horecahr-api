##employer.getCandidate  - Получение информации о кандидате

Запрос
```
{"jsonrpc": "2.0", "method": "employer.getCandidate", "params": [{"token" : "Iz2GoecY2t-NS3OukHjvB1h_XNgO68eP", "candidate_id" : 2}], "id": 1}
```

Удачный ответ
```
[
  {
    "jsonrpc": "2.0",
    "id": 3,
    "result": {
      "birthdate": "2003-04-04",
      "city": null,
      "contact_email": "contact@me.ru",
      "contact_phone": null,
      "about": "Я тестовый пользователь и очень много работаю пока проект не запустят.\nДо этого меня конечно же не было, но собственно об этом никто и не знал",
      "rating_count": "1",
      "rating": 4,
      "education": "Военмех, специальность \"Лучше всех\", лет:5\nШкола переподготовки, специальность \"HTML и CSS\", лет:2 месяца\n",
      "experience": "HorecaHelp Разработчик.\nFabrica Saitov Единственный разработчик.\n",
      "can_rate": true
    }
  }
]
```