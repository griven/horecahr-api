##employer.getWithoutReplyVacancy - Получение списка вакансий на которые нет откликов

Запрос
```
{"jsonrpc": "2.0", "method": "employer.getWithoutReplyVacancy", "params": [{"token" : "Iz2GoecY2t-NS3OukHjvB1h_XNgO68eP"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "94",
      "salary": "2000",
      "salary_type": "2",
      "start_at": "0000-00-00 00:00:00",
      "finish_at": "2016-12-22 00:00:00",
      "status": "0",
      "house_id": "1",
      "specialist_id": null,
      "position_id": "1",
      "employer_id": null,
      "position_name": "Повар",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    },
    {
      "id": "93",
      "salary": "2000",
      "salary_type": "2",
      "start_at": "0000-00-00 00:00:00",
      "finish_at": "2016-12-22 00:00:00",
      "status": "0",
      "house_id": "1",
      "specialist_id": null,
      "position_id": "1",
      "employer_id": null,
      "position_name": "Повар",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    }
  ]
}
```