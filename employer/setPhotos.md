##employer.setPhotos    -POST Добавление фотографий заведения

###параметры:
- file[0] - файл, обязательный. Изображение
- file[1] - файл, необязательный. Изображение
- file[2] - файл, необязательный. Изображение

Удачный ответ
```
[
  "http://hr2.stg.horecahelp.ru//uploads/files/6/2016/04/12/570ce024e09c6.jpeg",
  "http://hr2.stg.horecahelp.ru//uploads/files/6/2016/04/12/570ce025a2007.jpeg"
]
```