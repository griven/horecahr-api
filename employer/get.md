##employer.get                    - Получение информации о заведении

Запрос
```
{"jsonrpc": "2.0", "method": "employer.get", "params": [{"token" : "Iz2GoecY2t-NS3OukHjvB1h_XNgO68eP"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "74",
  "result": {
    "id": 1,
    "title": "На невском",
    "address": "Невский 123",
    "phone": "8 (812) 123-45-67",
    "logoUrl": "http://main.stg.horecahelp.ru//uploads/files/6/2016/03/11/56e2c3a823f79.jpg",
    "type": "Клуб",
    "photos": [],
    "balance": 0
  }
}
```