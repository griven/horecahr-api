##employer.getConfirmedVacancy - Получение списка вакансий с подтвержденым кандидатом

Запрос
```
{"jsonrpc": "2.0", "method": "employer.getConfirmedVacancy", "params": [{"token" : "Iz2GoecY2t-NS3OukHjvB1h_XNgO68eP"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "44",
      "salary": "200",
      "salary_type": "0",
      "start_at": "2015-12-28 19:00:00",
      "finish_at": "2015-12-29 19:00:00",
      "status": "2",
      "house_id": "1",
      "specialist_id": null,
      "position_id": "2",
      "employer_id": null,
      "position_name": "Официант",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    },
    {
      "id": "37",
      "salary": "200",
      "salary_type": "0",
      "start_at": "2015-12-28 19:00:00",
      "finish_at": "2015-12-29 19:00:00",
      "status": "2",
      "house_id": "1",
      "specialist_id": null,
      "position_id": "2",
      "employer_id": null,
      "position_name": "Официант",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    }
  ]
}
```