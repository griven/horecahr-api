##employer.getRepliedVacancy - Получение списка вакансий на которые есть отклики

Запрос
```
{"jsonrpc": "2.0", "method": "employer.getRepliedVacancy", "params": [{"token" : "Iz2GoecY2t-NS3OukHjvB1h_XNgO68eP"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    {
      "id": "43",
      "salary": "200",
      "salary_type": "1",
      "start_at": "2015-12-24 19:27:00",
      "finish_at": "2015-12-24 19:50:00",
      "status": "1",
      "house_id": "1",
      "specialist_id": null,
      "position_id": "1",
      "employer_id": null,
      "position_name": "Повар",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    },
    {
      "id": "36",
      "salary": "200",
      "salary_type": "1",
      "start_at": "2015-12-24 19:27:00",
      "finish_at": "2015-12-24 19:50:00",
      "status": "1",
      "house_id": "1",
      "specialist_id": null,
      "position_id": "1",
      "employer_id": null,
      "position_name": "Повар",
      "work_dates": [],
      "title": "Быстро и вкусно",
      "address": "Литейный 26",
      "latitude": "123",
      "longitude": "34.3"
    }
  ]
}
```