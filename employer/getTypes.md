##employer.getTypes      - Получение типов заведений

Запрос
```
{"jsonrpc":"2.0","method":"employer.getTypes","params":[{"token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST"}],"id":"74"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "74",
  "result": [
    {
      "id": 1,
      "name": "Ресторан"
    },
    {
      "id": 2,
      "name": "Кафе"
    },
    ...
  ]
}
```