##support.info                   -Получение информации о службе поддержки

Запрос
```
{"jsonrpc": "2.0", "method": "support.info", "params": [{"token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "phone": "8 (812) 245-16-17",
    "e-mail": "info@horecahelp.ru",
    "site-url": "horecahelp.ru"
  }
}
```