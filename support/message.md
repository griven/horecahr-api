##support.message -Отправка сообщения в службу поддержки

Запрос
```
{"jsonrpc": "2.0", "method": "support.message", "params": [{"token" : "wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "message" : "asd"}], "id": 1} 
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": true
}
```