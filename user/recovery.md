##user.recovery                      - Восстановление пароля

###параметры:
- email - строка, обязательный.

Запрос
```
{"jsonrpc": "2.0", "method": "user.recovery", "params": [{ "email" : "aa@aa.aa"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": true
}
```

Неудачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "email": [
      "Нет пользователя с таким email"
    ]
  }
}
```