##user.addFavorite - Добавление пользователя в избранное

###параметры:
- user_id - число, обязательный. Id пользователя которого добавляют в избранное

Запрос
```
[
  {
    "jsonrpc": "2.0", 
    "method": "user.addFavorite", 
    "params": 
    [
      {
        "token":"xpIArzV-guJ50GfINq1ESUGR5gInGrST",
        "user_id":5
      }
    ],
    "id": 3
}
]
```

Удачный ответ
```
[
  {
    "jsonrpc": "2.0",
    "id": 3,
    "result": true
  }
]
```
