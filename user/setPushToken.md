##user.setPushToken - Установка пуш токена

###параметры:
- push_token - строка, обязательный. Токен по которому можно будет отправлять пуш сообщения
- device_id - строка, необязательный. Id устройства 

Запрос
```
{"jsonrpc":"2.0","method":"user.setPushToken","params":[{"token":"wPHmGhu8vwHlH20Z4JmnDBSr9mMb9ulS", "push_token" : "abc"}],"id":"12"}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": "12",
  "result": true
}
```
