##user.logout                     - Отмена авторизации

Запрос
```
{"jsonrpc": "2.0", "method": "user.logout", "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": true
}
```