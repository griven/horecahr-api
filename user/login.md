##user.login                      - Авторизация пользователя

###параметры:
- email - строка, обязательный.
- password - строка, обязательный.

Запрос
```
{"jsonrpc": "2.0", "method": "user.login", "params": [{"email" : "aa@aa.aa", "password" : "qwerty"}], "id": 1}
```

Удачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": "aO2O-G3kPiumfBTR00oLZnKCh23-7Q_Y"
}
```

Неудачный ответ
```
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "password": [
      "Invalid email or password"
    ]
  }
}
```